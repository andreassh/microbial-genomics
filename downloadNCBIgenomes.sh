#!/bin/bash

available_organisms=("archaea" "bacteria" "fungi" "invertebrate" "metagenomes" "other" "plant" "protozoa" "vertebrate_mammalian" "vertebrate_other" "viral")


echo -e "This scripts downloads all complete genomes from NCBI for the given organism and also merges them into one FASTA file. Please write the name of the organism, available options are as follows: \n${available_organisms[*]}"
read organism


if [[ " ${available_organisms[*]} " =~ " ${organism} " ]]; then
	echo "Starting to download genomes for ${organism}"
	mkdir FASTA_${organism}
 	curl ftp://ftp.ncbi.nlm.nih.gov/genomes/genbank/${organism}/assembly_summary.txt | awk -F '\t' '{if($12=="Complete Genome") print $20}' | awk -F/ '{print $0"/"$NF"_genomic.fna.gz"}' | xargs -I{} -n1 -P8 wget -nc -P FASTA_${organism} {}

 	echo "Unpacking FASTA files and merging them into one (all_complete_genomes_${organism}.fa)"
	find FASTA_${organism} -type f -exec gunzip {} +

	find FASTA_${organism} -type f -exec cat {} + > all_complete_genomes_${organism}.fa
fi

echo "Finished"
