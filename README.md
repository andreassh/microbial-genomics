# Microbial Genomics

Useful stuff for working on microbial genomics.

## Files
[downloadNCBIgenomes.sh](downloadNCBIgenomes.sh) - Download complete genomes from NCBI (either for archaea, bacteria, fungi, invertebrate, metagenomes, other, plant, protozoa, vertebrate_mammalian, vertebrate_other,viral) and pack them into one combined FASTA file.
